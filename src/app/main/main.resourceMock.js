(function() {
  'use strict';

  angular
    .module('vctest')
    .service('executeQueryMock', executeQueryMock);

  /** @ngInject */
  function executeQueryMock($q) {
    this.resetDeferred = function () {
      this.deferred = $q.defer();
    };
  }

  executeQueryMock.prototype.schema = function () {
    this.resetDeferred();
    this.deferred.resolve([
      {
        t1: {
          field1: String,
          field2: String
        }
      },
      {
        t2: {
          field1: String,
          field2: String,
          userid: Number,
          username: String
        }
      }
    ]);
    return {
      $promise: this.deferred.promise
    }
  };

  executeQueryMock.prototype.post = function () {
    this.resetDeferred();
    this.deferred.resolve({
      results: [
        {'nifty': 'somevalue', 'bar': 'sombar'},
        {'nifty': 'somevalue2', 'bar': 'somotherbar'}
      ]
    });
    return {
      $promise: this.deferred.promise
    }
  };

})();
