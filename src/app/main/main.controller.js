(function () {
  //'use strict';

  angular
      .module('vctest')
      .controller('MainController', MainController);

  /** @ngInject */
  function MainController($log, $mdDialog, $scope, executeQueryMock, lodash) {
    var vm = this;

    executeQueryMock.schema().$promise.then(function (response) {
      vm.tables = response;

      // I wouldn't normally write code like this as I'd expect some sort of
      // better data from the server!
      // This is just so that I can show the autocomplete really
      vm.tableFields = lodash.reduceRight( lodash.reduceRight(
          lodash.map(response, function (table) {
            var tableName = lodash.keys(table)[0];
            return lodash.map(table, function (field) {
              return lodash.map(
                  lodash.keys(field),
                  function (key) { return [tableName, key].join('.')});
            })
          }), function(a, b) { return a.concat(b); }, []), function(a, b) { return a.concat(b); }, []);
    });

    vm.chips = [];
    vm.query = [];
    vm.queryParsed = '';
    vm.error = {};
    vm.showModal = false;

    vm.queryDisplay = function () {
      vm.queryParsed = lodash.map(vm.query, function (part) {
        return (lodash.has(part, 'string')) ? decodeURIComponent(part.display) : part.display;
      }).join(' ');
      return vm.queryParsed;
    };

    $scope.$watchCollection(function () {
      return vm.chips;
    }, function(newval) {
      if (newval.length > 0) {
        var newvalCopy = lodash.clone(newval);
        vm.appendQuery(newvalCopy.pop());
      }
    });

    vm.appendQuery = function (data) {
      var parsedData;
      if (lodash.has(data, 'table')) {
        parsedData = "'" + data.table + "'";
      } else if (lodash.has(data, 'syntax')) {
        parsedData = (data.syntax === 'equals') ? '==' : data.syntax;
      } else if (lodash.has(data, 'field')) {
        parsedData = [data['table-name'], data.field].join('.');
      } else if (angular.isString(data)) {
        parsedData = encodeURIComponent(data);
        data = {'string': data};
      }

      if (data) {
        data.display = parsedData;
        vm.query.push(data);
      }

      vm.queryDisplay();
      return vm.query;
    };

    vm.querySyntax = [
      'select', 'as', 'from', 'where', 'equals'
    ];

    vm.submitQuery = function (ev) {

      var confirm = $mdDialog.confirm()
          .title('Would you like to submit this query?')
          .content(vm.queryParsed)
          .targetEvent(ev)
          .ok('Please do it!')
          .cancel('Modify the query');
      $mdDialog.show(confirm).then(function () {

        executeQueryMock
            .post({query: vm.query})
            .$promise.then(function (response) {
              vm.results = response.results;
        });
      });
    };

    // autocomplete
    vm.selectedItem = '';
    vm.searchText = '';

    vm.fieldSearch = function (tableFields) {
      return vm.searchText.length > 0 ?  lodash.filter(tableFields, function (field) {
        return field.indexOf(vm.searchText.toLowerCase()) > -1;
      }) : tableFields;
    };

    vm.searchTextChange = function (text) {
      $log.info('Text changed to ' + text);
    };

    vm.selectedItemChange = function (item) {
      $log.info('Item changed to ' + JSON.stringify(item));
      vm.appendQuery(item);
    };

  }
})();
