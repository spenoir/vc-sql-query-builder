(function () {
  'use strict';

  angular
      .module('vctest')
      .factory('executeQuery', executeQuery);


  /** @ngInject */
  function executeQuery($resource) {
    return $resource('/some-hive-backend', {}, {
      post: {
        method: 'POST',
        params: {
          type: 'SQL'
        }
      },
      schema: {
        method: 'GET'
      }
    });
  }
})();
