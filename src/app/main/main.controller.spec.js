(function () {
  'use strict';

  describe('MainController', function () {
    var main;
    var $rootScope;

    beforeEach(module('vctest'));
    beforeEach(inject(function (_$controller_, _$rootScope_) {
      $rootScope = _$rootScope_;

      main = _$controller_('MainController', {
        '$scope': $rootScope.$new()
      });

      $rootScope.$apply();
    }));

    it('set the schema', function () {
      expect(main.tables.length).toEqual(2);
      expect(main.tableFields.length).toEqual(6);
    });

    it('has correct logic on drop of table name', function () {
      main.appendQuery({'table': 't1'});
      expect(main.query.length).toEqual(1);
    });

    it('has correct logic on drop of sql syntax', function () {
      main.appendQuery({'syntax': 'select'});
      expect(main.query.length).toEqual(1);
      expect(main.query[0]).toEqual({syntax: 'select', display: 'select'});

      main.appendQuery({'syntax': 'equals'});
      expect(main.query.length).toEqual(2);
      expect(main.query[1].display).toEqual('==');
    });

    it('has correct logic on drop of table attrs', function () {
      main.appendQuery({'table-name': 't1', 'field': 'field1'});
      expect(main.query.length).toEqual(1);
      expect(main.query[0]).toEqual({
        'table-name': 't1', 'field': 'field1', display: 't1.field1'
      });
    });

    it('can build the query string correctly', function () {
      main.appendQuery({'syntax': 'select'});
      main.appendQuery({'table-name': 't1', 'field': 'field1'});
      main.appendQuery({'syntax': 'from'});
      main.appendQuery({'table': 't1'});
      main.appendQuery({'syntax': 'where'});
      main.appendQuery({'table-name': 't2', 'field': 'username'});
      main.appendQuery({'syntax': 'equals'});
      main.appendQuery('"lance"');
      expect(main.queryParsed).toEqual('select t1.field1 from \'t1\' where t2.username == "lance"');
    });

    it('can search correctly', function () {
      main.searchText = 't1';
      expect(main.fieldSearch([
        "t1.field1","t1.field2",
        "t2.field1","t2.field2","t2.userid","t2.username"]).length).toEqual(2);

      main.searchText = '';
      expect(main.fieldSearch([
        "t1.field1","t1.field2",
        "t2.field1","t2.field2","t2.userid","t2.username"]).length).toEqual(6);
    })

  });
})();
