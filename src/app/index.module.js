(function() {
  'use strict';

  angular
    .module('vctest', ['ngAnimate',
      'ngResource', 'ngRoute', 'ngMaterial', 'ang-drag-drop', 'ngLodash']);

})();
