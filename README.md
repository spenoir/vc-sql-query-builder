VC Query Builder
----------------

Install
=======

`npm install`
`bower install`

Serve
=====

`gulp serve`

Yeoman Generator
=========
  
    https://github.com/Swiip/generator-gulp-angular/blob/master/docs/usage.md

Run the tests
=============

`gulp test` or `npm test`


Synopsis
========

I've built a prototype of one idea that I thought could work. I'm not sure who the target audience
for this query builder would be but it would probably suit someone with some knowledge of sql. 
There is currently no validation on sql strings generated, I think it would benefit from validation in 
a production environment.

I reckon there could be different solutions that better satisfy the requirement: 
'representation of the query separate of the sql string'.

I would consider refactoring my use of Material Design chips too in production code. I would also write some e2e tests using Protractor which I didn't get round to doing unfortunately.

Schemas
=======

I stuck to the basic schemas you gave me:

    t1 {
        field1: String
        field2: String
    },
    t2 {
        field1: String,
        field2: String, 
        userid: Number
        username: String
    }
  
the service we're talking to expects
POST with a json payload of 

    {
    
    type: 'SQL', 
    
    query: 'some query'
    
    }

it responds 200 with

    {
    
        results: [
    
            {'nifty': 'somevalue', 'bar': 'sombar'}, 
    
            {'nifty': 'somevalue2', 'bar': 'somotherbar'},
    
        ]
    
    }